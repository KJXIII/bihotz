import React from 'react';
import App from './App';
import { BrowserRouter, Route } from 'react-router-dom';
import './App.css';
import { render } from 'react-snapshot';

render(
  <BrowserRouter>
    <Route path="/" component={App} />
  </BrowserRouter>,
  document.getElementById('root')
);
