import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import './home.scss';
import '../../ext';

import $ from 'jquery';

import { Helmet } from 'react-helmet';

import P1 from '../assets/marjorie.jpeg';
import ZEGO from '../assets/Zego/ZegoV1.jpg';
import MIHI from '../assets/Mihi/MihiV1.jpg';
import XERRA from '../assets/Xerra/XerraV1.jpg';
import KUPELA from '../assets/Kupela/KupelaV1.jpg';

import ScrollReveal from 'scrollreveal';

import '@animated-burgers/burger-squeeze/dist/styles.css';

export default class home extends Component {
  componentDidMount() {
    setTimeout(function() {
      window.scrollTo(0, 0);
    }, 200);

    $('.arrow-down').click(function() {
      $('html,body').animate(
        {
          scrollTop: $('.arrow-down').offset().top - 80
        },
        'slow'
      );
    });
    if (window.matchMedia('(min-width: 820px)').matches) {
      ScrollReveal().reveal('.overview__heading--title', { delay: 300, duration: 600 });
      ScrollReveal().reveal('.overview__text--first', { delay: 400, duration: 600 });
      ScrollReveal().reveal('.overview__text--second', { delay: 500, duration: 700 });
      ScrollReveal().reveal('.overview__follow', { delay: 800, duration: 700 });
      ScrollReveal().reveal('.overview__SM', { delay: 900, duration: 700 });
    } else {
      ScrollReveal().reveal('.overview__heading--title', { delay: 300, duration: 600 });
      ScrollReveal().reveal('.overview__text--first', { delay: 400, duration: 600 });
      ScrollReveal().reveal('.overview__text--second', { delay: 500, duration: 700 });
      ScrollReveal().reveal('.overview__follow', { delay: 400, duration: 700 });
      ScrollReveal().reveal('.overview__SM', { delay: 400, duration: 700 });
    }

    ScrollReveal().reveal('.heading__title', { delay: 200, origin: 'left', distance: '20px', duration: 700 });
    ScrollReveal().reveal('.underline', { delay: 200, duration: 700, origin: 'left', distance: '50px' });

    ScrollReveal().reveal('.zego', { delay: 200, duration: 600 });
    ScrollReveal().reveal('.xerra', { delay: 200, duration: 900 });
    ScrollReveal().reveal('.mihi', { delay: 200, duration: 600 });
    ScrollReveal().reveal('.kupela', { delay: 200, duration: 900 });
  }

  render() {
    var land = {
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      backgroundSize: 'cover',
      backgroundImage: `url(${P1})`
    };

    return (
      <Fragment>
        <Helmet>
          <meta charset="UTF-8" />
          <title>Bihotz - La maroquinerie consciente</title>
          <meta name="title" content="Bihotz - La maroquinerie consciente" />
          <meta
            name="description"
            content="Marque de maroquinerie française haut de gamme, réalisée dans un atelier en France avec des matières innovantes, éco-responsables et veganes."
          />
          <meta property="og:type" content="website" />
          <meta property="og:url" content="https://bihotz.co" />
          <meta property="og:image" content={P1} />
        </Helmet>

        <div className="landing__hero" style={land}>
          <div className="menu">
            <div class="burger burger-squeeze">
              <div class="burger-lines"></div>
            </div>
          </div>
          <div className="hidden-menu">
            <div className="menu__container">
              <div className="menu__bags">
                <Link className="menu__link" to="/">
                  Accueil
                </Link>
                <Link className="menu__link" to="/sac/zego">
                  Zego
                </Link>
                <Link className="menu__link" to="/sac/kupela">
                  Kupela
                </Link>
                <Link className="menu__link" to="/sac/mihi">
                  Mihi
                </Link>
                <Link className="menu__link" to="/sac/xerra">
                  Xerra
                </Link>
              </div>
            </div>
          </div>
          <div className="landing__hero--filter" />
          <div className="bihotz">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 161 20">
              <g
                fill="#FFFFFF"
                fillRule="nonzero"
                stroke="none"
                strokeWidth="1"
                transform="translate(-848 -268) translate(848 268)"
              >
                <path d="M28.06 5.26L30.89 5.26 30.89 14.26 28.06 14.26z" />
                <path d="M52.48 1.19L55.35 1.19 55.35 6.35 60.64 6.35 60.64 1.19 63.5 1.19 63.5 14.19 60.64 14.19 60.64 9 55.35 9 55.35 14.24 52.48 14.24z" />
                <path d="M84.68 7.75a6.93 6.93 0 0113.86 0 6.93 6.93 0 01-13.86 0zm10.86 0a4 4 0 00-4-4.1 3.9 3.9 0 00-3.91 4.06 4 4 0 003.95 4.1 3.9 3.9 0 003.96-4.06z" />
                <path d="M122.19 3.83L118.19 3.83 118.19 1.19 129 1.19 129 3.83 125 3.83 125 14.23 122.13 14.23z" />
                <path d="M149.27 12.05L156.44 3.7 149.49 3.7 149.49 1.19 160.13 1.19 160.13 3.37 153 11.71 160.17 11.71 160.17 14.23 149.27 14.23z" />
                <circle cx="29.52" cy="1.58" r="1.58" />
                <circle cx="91.61" cy="18.37" r="1.58" />
                <path d="M8.74 8.91a4.58 4.58 0 00-.46-1.33 7.55 7.55 0 00-1-1.35A12.45 12.45 0 006.06 5a2.51 2.51 0 00.25-2.89 2.75 2.75 0 00-.71-.76 3.36 3.36 0 00-.88-.43A2.58 2.58 0 004 .77H0v13.79h4.48a4.22 4.22 0 003.76-2c.2-.266.345-.568.43-.89a3.73 3.73 0 00.16-.78v-.74a6.67 6.67 0 00-.09-1.24zM2.45 7.64L4 6.71c.297.178.575.386.83.62a5 5 0 01.81.93c.21.312.381.647.51 1 .115.304.176.625.18.95a1.86 1.86 0 01-.17.79 2.33 2.33 0 01-.46.64 2.12 2.12 0 01-.64.43 1.75 1.75 0 01-.69.15H2.45V7.64zM3.7 3.92a1.63 1.63 0 01-.55.49 5.23 5.23 0 01-.71.44V3.28h.89c.25 0 .67.14.37.64z" />
              </g>
            </svg>
          </div>

          <svg className="arrow-down" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
            <path d="M23.25 7.311L12.53 18.03a.749.749 0 01-1.06 0L.75 7.311" className="cls-1" />
          </svg>
        </div>
        <div className="main-wrapper">
          <div className="overview">
            <div className="overview__heading">
              <h3>Maroquinerie Consciente</h3>
              <h2 className="overview__heading--title">Maroquinerie respectueuse de l'environnement et d'origine non animale.</h2>

              <div className="overview__follow">Suivez - nous</div>
              <div className="overview__SM">
                <a href="https://www.instagram.com/bihotz_btz/">
                  <svg className="overview__insta" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <path d="M12 2.163c3.204 0 3.584.012 4.85.07 3.252.148 4.771 1.691 4.919 4.919.058 1.265.069 1.645.069 4.849 0 3.205-.012 3.584-.069 4.849-.149 3.225-1.664 4.771-4.919 4.919-1.266.058-1.644.07-4.85.07-3.204 0-3.584-.012-4.849-.07-3.26-.149-4.771-1.699-4.919-4.92-.058-1.265-.07-1.644-.07-4.849 0-3.204.013-3.583.07-4.849.149-3.227 1.664-4.771 4.919-4.919 1.266-.057 1.645-.069 4.849-.069zm0-2.163c-3.259 0-3.667.014-4.947.072-4.358.2-6.78 2.618-6.98 6.98-.059 1.281-.073 1.689-.073 4.948 0 3.259.014 3.668.072 4.948.2 4.358 2.618 6.78 6.98 6.98 1.281.058 1.689.072 4.948.072 3.259 0 3.668-.014 4.948-.072 4.354-.2 6.782-2.618 6.979-6.98.059-1.28.073-1.689.073-4.948 0-3.259-.014-3.667-.072-4.947-.196-4.354-2.617-6.78-6.979-6.98-1.281-.059-1.69-.073-4.949-.073zm0 5.838c-3.403 0-6.162 2.759-6.162 6.162s2.759 6.163 6.162 6.163 6.162-2.759 6.162-6.163c0-3.403-2.759-6.162-6.162-6.162zm0 10.162c-2.209 0-4-1.79-4-4 0-2.209 1.791-4 4-4s4 1.791 4 4c0 2.21-1.791 4-4 4zm6.406-11.845c-.796 0-1.441.645-1.441 1.44s.645 1.44 1.441 1.44c.795 0 1.439-.645 1.439-1.44s-.644-1.44-1.439-1.44z" />
                  </svg>
                </a>
                <a href="https://www.facebook.com/bihotz.btz">
                  <svg
                    className="overview__fb"
                    width="32px"
                    height="33px"
                    viewBox="0 0 32 33"
                    version="1.1"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                      <g id="Homepage" transform="translate(-203.000000, -1359.000000)" fill="#000000" fillRule="nonzero">
                        <g id="facebook" transform="translate(203.000000, 1359.000000)">
                          <path
                            d="M32,16.5980707 C32,7.64095004 24.8387097,0.38372093 16,0.38372093 C7.16129032,0.38372093 0,7.64095004 0,16.5980707 C0,24.6908618 5.85096774,31.3988952 13.5,32.6162791 L13.5,21.2851946 L9.43548387,21.2851946 L9.43548387,16.5980707 L13.5,16.5980707 L13.5,13.0256833 C13.5,8.96228879 15.8870968,6.7177782 19.5432258,6.7177782 C21.2941935,6.7177782 23.1251613,7.03421954 23.1251613,7.03421954 L23.1251613,11.0224265 L21.1070968,11.0224265 C19.12,11.0224265 18.5,12.2725006 18.5,13.5546111 L18.5,16.5980707 L22.9374194,16.5980707 L22.2277419,21.2851946 L18.5,21.2851946 L18.5,32.6162791 C26.1490323,31.3988952 32,24.6908618 32,16.5980707 Z"
                            id="Path"
                          ></path>
                        </g>
                      </g>
                    </g>
                  </svg>
                </a>
              </div>
            </div>
            <div className="overview__text">
              <h3>Qui Sommes-nous ?</h3>
              <p className="overview__text--first">
                Nous souhaitons prouver que la maroquinerie consciente peut être une véritable alternative à la maroquinerie
                conventionnelle. Nous nous engageons pour des conditions de travail optimales dans des ateliers français, contre
                la souffrance animale et pour un faible impact environnemental. Nous trouvons des alternatives toujours plus
                responsables permettant de réduire à notre échelle les conséquences néfastes de l’industrie de la mode sur la
                planète, les conditions de travail et les animaux.
              </p>
              <p className="overview__text--second">
                Nous vous proposons un travail d’excellence grâce à un savoir-faire français provenant d’un atelier de
                maroquinerie labellisé « <i>entreprise du patrimoine vivant</i> ».
              </p>
            </div>
          </div>

          <div className="heading">
            <h1 className="heading__title">Notre premiere collection</h1>
            <div className="underline" />
          </div>

          <div className="bags">
            <div className="bags__left">
              <Link to="/sac/zego" className="bag__container zego">
                <div className="bag__image">
                  <div className="bagimg__wrap">
                    <img alt="" className="bagimg" src={ZEGO} />
                  </div>

                  <div className="bag__border" />
                </div>
                <div className="bag__text">
                  <div className="bag__line" />
                  <div className="bag__infos">
                    <h2 className="bag__name">Zego</h2>
                    <div className="bag__description">Le sac seau, revisité par Bihotz.</div>
                  </div>
                </div>
              </Link>

              <Link to="/sac/mihi" className="bag__container mihi">
                <div className="bag__image">
                  <div className="bagimg__wrap">
                    <img alt="" className="bagimg" src={MIHI} />
                  </div>
                  <div className="bag__border" />
                </div>
                <div className="bag__text">
                  <div className="bag__line" />
                  <div className="bag__infos">
                    <h2 className="bag__name">Mihi</h2>
                    <div className="bag__description">La pochette de soirée, revisitée par Bihotz.</div>
                  </div>
                </div>
              </Link>
            </div>

            <div className="bags__right">
              <Link to="/sac/kupela" className="bag__container kupela">
                <div className="bag__image">
                  <div className="bagimg__wrap">
                    <img alt="" className="bagimg" src={KUPELA} />
                  </div>
                  <div className="bag__border--down" />
                </div>
                <div className="bag__text">
                  <div className="bag__line" />
                  <div className="bag__infos">
                    <h2 className="bag__name">Kupela</h2>
                    <div className="bag__description">Le sac de sport rétro, revisité par Bihotz.</div>
                  </div>
                </div>
              </Link>

              <Link to="/sac/xerra" className="bag__container xerra">
                <div className="bag__image">
                  <div className="bagimg__wrap">
                    <img alt="" className="bagimg" src={XERRA} />
                  </div>
                  <div className="bag__border--down" />
                </div>
                <div className="bag__text">
                  <div className="bag__line" />
                  <div className="bag__infos">
                    <h2 className="bag__name">Xerra</h2>
                    <div className="bag__description">Le sac grille-pain, revisité par Bihotz.</div>
                  </div>
                </div>
              </Link>
            </div>
          </div>
        </div>
        <div className="footer">
          <div className="footer__container">
            <h3 className="footer__name">
              <span>BIHOTZ</span>, la maroquinerie consciente
            </h3>

            <div className="footer__right">
              <div className="footer__SM">
                <a href="https://www.instagram.com/bihotz_btz/">
                  <svg className="footer__insta" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <path d="M12 2.163c3.204 0 3.584.012 4.85.07 3.252.148 4.771 1.691 4.919 4.919.058 1.265.069 1.645.069 4.849 0 3.205-.012 3.584-.069 4.849-.149 3.225-1.664 4.771-4.919 4.919-1.266.058-1.644.07-4.85.07-3.204 0-3.584-.012-4.849-.07-3.26-.149-4.771-1.699-4.919-4.92-.058-1.265-.07-1.644-.07-4.849 0-3.204.013-3.583.07-4.849.149-3.227 1.664-4.771 4.919-4.919 1.266-.057 1.645-.069 4.849-.069zm0-2.163c-3.259 0-3.667.014-4.947.072-4.358.2-6.78 2.618-6.98 6.98-.059 1.281-.073 1.689-.073 4.948 0 3.259.014 3.668.072 4.948.2 4.358 2.618 6.78 6.98 6.98 1.281.058 1.689.072 4.948.072 3.259 0 3.668-.014 4.948-.072 4.354-.2 6.782-2.618 6.979-6.98.059-1.28.073-1.689.073-4.948 0-3.259-.014-3.667-.072-4.947-.196-4.354-2.617-6.78-6.979-6.98-1.281-.059-1.69-.073-4.949-.073zm0 5.838c-3.403 0-6.162 2.759-6.162 6.162s2.759 6.163 6.162 6.163 6.162-2.759 6.162-6.163c0-3.403-2.759-6.162-6.162-6.162zm0 10.162c-2.209 0-4-1.79-4-4 0-2.209 1.791-4 4-4s4 1.791 4 4c0 2.21-1.791 4-4 4zm6.406-11.845c-.796 0-1.441.645-1.441 1.44s.645 1.44 1.441 1.44c.795 0 1.439-.645 1.439-1.44s-.644-1.44-1.439-1.44z" />
                  </svg>
                </a>
                <a href="https://www.facebook.com/bihotz.btz">
                  <svg
                    className="footer__fb"
                    width="32px"
                    height="33px"
                    viewBox="0 0 32 33"
                    version="1.1"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                      <g id="Homepage" transform="translate(-203.000000, -1359.000000)" fill="#000000" fillRule="nonzero">
                        <g id="facebook" transform="translate(203.000000, 1359.000000)">
                          <path
                            d="M32,16.5980707 C32,7.64095004 24.8387097,0.38372093 16,0.38372093 C7.16129032,0.38372093 0,7.64095004 0,16.5980707 C0,24.6908618 5.85096774,31.3988952 13.5,32.6162791 L13.5,21.2851946 L9.43548387,21.2851946 L9.43548387,16.5980707 L13.5,16.5980707 L13.5,13.0256833 C13.5,8.96228879 15.8870968,6.7177782 19.5432258,6.7177782 C21.2941935,6.7177782 23.1251613,7.03421954 23.1251613,7.03421954 L23.1251613,11.0224265 L21.1070968,11.0224265 C19.12,11.0224265 18.5,12.2725006 18.5,13.5546111 L18.5,16.5980707 L22.9374194,16.5980707 L22.2277419,21.2851946 L18.5,21.2851946 L18.5,32.6162791 C26.1490323,31.3988952 32,24.6908618 32,16.5980707 Z"
                            id="Path"
                          ></path>
                        </g>
                      </g>
                    </g>
                  </svg>
                </a>
              </div>
              <a href="mailto:contact@bihotz.co" className="footer__mail">
                contact@bihotz.co
              </a>
              <div className="footer__boss">
                Site web fièrement réalisé par <a href="mailto:martin@miza.io">Mizā Studio</a>.
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}
