import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import '../bag.scss';
import '../../ext';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';

import { Helmet } from 'react-helmet';
import ScrollReveal from 'scrollreveal';

import XERRA from '../assets/Xerra/XerraV1.jpg';
import XERRA2 from '../assets/Xerra/XerraV2.jpg';
import XERRA3 from '../assets/Xerra/XerraV3.jpg';
import XERRAS1 from '../assets/Xerra/XerraS1.jpg';
import XERRAS2 from '../assets/Xerra/XerraS2.jpg';
import XERRAS3 from '../assets/Xerra/XerraS3.jpg';
import XERRAS4 from '../assets/Xerra/XerraS4.jpg';

import MetaXerra from '../assets/xerra.jpg';

const images = [XERRA, XERRA2, XERRA3];
const images2 = [XERRAS1, XERRAS2, XERRAS3, XERRAS4];

export default class xerra extends Component {
  constructor(props) {
    super(props);

    this.state = {
      photoIndex: 0,
      isOpen: false,
      isOpen2: false
    };
  }

  componentDidMount() {
    setTimeout(function() {
      window.scrollTo(0, 0);
    }, 200);

    ScrollReveal().reveal('.item__container', { delay: 300, duration: 600 });
    ScrollReveal().reveal('.item__pics--container', { delay: 300, duration: 600 });
  }

  render() {
    const { photoIndex, isOpen, isOpen2 } = this.state;
    var land = {
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      backgroundSize: 'cover',
      backgroundImage: `url(${MetaXerra})`
    };

    return (
      <Fragment>
        <Helmet>
          <meta charset="UTF-8" />
          <title>Xerra - Le sac grille-pain revisité par Bihotz</title>
          <meta name="title" content="Xerra - Le sac grille-pain revisité par Bihotz" />
          <meta
            name="description"
            content="Ultra-compact tout en étant ultra-pratique, le sac Xerra est un véritable sac-bijou."
          />
          <meta property="og:type" content="website" />
          <meta property="og:url" content="https://bihotz.co/xerra" />
          <meta property="og:image" content={MetaXerra} />
        </Helmet>

        <div className="item__hero" style={land}>
          <div className="item__hero--filter" />
          <h1 className="item__hero--name">Xerra</h1>
          <div className="menu">
            <div className="burger burger-squeeze">
              <div className="burger-lines"></div>
            </div>
          </div>
          <div className="hidden-menu">
            <div className="menu__container">
              <div className="menu__bags">
                <Link className="menu__link" to="/">
                  Accueil
                </Link>
                <Link className="menu__link" to="/sac/zego">
                  Zego
                </Link>
                <Link className="menu__link" to="/sac/kupela">
                  Kupela
                </Link>
                <Link className="menu__link" to="/sac/mihi">
                  Mihi
                </Link>
                <Link className="menu__link" to="/sac/xerra">
                  Xerra
                </Link>
              </div>
            </div>
          </div>

          <Link to="/">
            <svg className="arrow-left" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
              <path d="M23.25 7.311L12.53 18.03a.749.749 0 01-1.06 0L.75 7.311" className="cls-1" />
            </svg>
          </Link>
        </div>

        <div className="main-wrapper">
          <div className="wrapper">
            <div className="wrapper__left">
              <div className="item__container">
                <div className="item__border"></div>
                <div className="item__top">
                  <h1 className="item__name">Xerra</h1>
                  <h3 className="item__description">
                    Le mini-sac Xerra a tout d’un grand. Assez petit pour se faufiler partout, assez grand pour contenir
                    l’essentiel, c’est notre coup de coeur! Avec ses superbes chaines et ses marquages dorés, c’est un vrai
                    sac-bijou. Intérieur doublé avec une poche plaquée.
                  </h3>
                </div>
                <div className="item__bottom">
                  <div className="item__colors">
                    <h4>Coloris</h4>
                    <div className="item__colors--container">
                      <div className="item__colors--ex xerra-black"></div>
                      <div className="item__colors--ex xerra-brown"></div>
                      <div className="item__colors--ex xerra-blue"></div>
                      <div className="item__colors--ex xerra-red"></div>
                    </div>
                  </div>
                  <div className="item__dimensions">
                    <h4>Dimensions</h4>
                    <span>23cm x 16cm x 7,5cm</span>
                  </div>
                </div>
              </div>
            </div>
            <div className="wrapper__right">
              <div className="item__pics--container">
                <img alt="" src={XERRA} onClick={() => this.setState({ isOpen: true })} className="item__pics--big"></img>
                <div className="item__pics--bottom">
                  <img alt="" onClick={() => this.setState({ isOpen: true })} src={XERRA2} className="item__pics--small" />
                  <img alt="" onClick={() => this.setState({ isOpen: true })} src={XERRA3} className="item__pics--small" />
                </div>
              </div>
            </div>
          </div>
          <div className="bag__composition">
            <h3>Composition de Xerra</h3>
            <h4>
              <span>Extérieur :</span> Bi-matière Ureko issue de bouteilles PET recyclées/Suede issue de matériaux
              post-consommation recyclés
            </h4>
            <h4>
              <span>Intérieur :</span> Sergé composé de 36% de coton bio GOTS et de 64% de polyester recyclé
            </h4>
          </div>
          <div className="item__order">
            <div className="item__line"></div>
            <a className="item__button" href="https://www.tudigo.co/don/bihotz-la-maroquinerie-consciente">
              Pré-commandez <span>Xerra</span>
            </a>
            <div className="item__line"></div>
          </div>

          <div className="bag__shooting">
            <img alt="" src={XERRAS1} onClick={() => this.setState({ isOpen2: true })} className="bag__landscape" />
            <div className="bag__portraits">
              <img alt="" className="bag__portraits--item" onClick={() => this.setState({ isOpen2: true })} src={XERRAS2} />
              <img alt="" className="bag__portraits--item" onClick={() => this.setState({ isOpen2: true })} src={XERRAS4} />
            </div>
            <img alt="" src={XERRAS3} onClick={() => this.setState({ isOpen2: true })} className="bag__landscape" />
          </div>

          <div className="item__order">
            <div className="item__line"></div>
            <a className="item__button" href="https://www.tudigo.co/don/bihotz-la-maroquinerie-consciente">
              Pré-commandez <span>Xerra</span>
            </a>
            <div className="item__line"></div>
          </div>
        </div>

        {isOpen && (
          <Lightbox
            mainSrc={images[photoIndex]}
            nextSrc={images[(photoIndex + 1) % images.length]}
            prevSrc={images[(photoIndex + images.length - 1) % images.length]}
            onCloseRequest={() => this.setState({ isOpen: false })}
            onMovePrevRequest={() =>
              this.setState({
                photoIndex: (photoIndex + images.length - 1) % images.length
              })
            }
            onMoveNextRequest={() =>
              this.setState({
                photoIndex: (photoIndex + 1) % images.length
              })
            }
          />
        )}
        {isOpen2 && (
          <Lightbox
            mainSrc={images2[photoIndex]}
            nextSrc={images2[(photoIndex + 1) % images2.length]}
            prevSrc={images2[(photoIndex + images2.length - 1) % images2.length]}
            onCloseRequest={() => this.setState({ isOpen2: false })}
            onMovePrevRequest={() =>
              this.setState({
                photoIndex: (photoIndex + images2.length - 1) % images2.length
              })
            }
            onMoveNextRequest={() =>
              this.setState({
                photoIndex: (photoIndex + 1) % images2.length
              })
            }
          />
        )}

        <div className="footer">
          <div className="footer__container">
            <h3 className="footer__name">
              <span>BIHOTZ</span>, la maroquinerie consciente
            </h3>

            <div className="footer__right">
              <div className="footer__SM">
                <a href="https://www.instagram.com/bihotz_btz/">
                  <svg className="footer__insta" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <path d="M12 2.163c3.204 0 3.584.012 4.85.07 3.252.148 4.771 1.691 4.919 4.919.058 1.265.069 1.645.069 4.849 0 3.205-.012 3.584-.069 4.849-.149 3.225-1.664 4.771-4.919 4.919-1.266.058-1.644.07-4.85.07-3.204 0-3.584-.012-4.849-.07-3.26-.149-4.771-1.699-4.919-4.92-.058-1.265-.07-1.644-.07-4.849 0-3.204.013-3.583.07-4.849.149-3.227 1.664-4.771 4.919-4.919 1.266-.057 1.645-.069 4.849-.069zm0-2.163c-3.259 0-3.667.014-4.947.072-4.358.2-6.78 2.618-6.98 6.98-.059 1.281-.073 1.689-.073 4.948 0 3.259.014 3.668.072 4.948.2 4.358 2.618 6.78 6.98 6.98 1.281.058 1.689.072 4.948.072 3.259 0 3.668-.014 4.948-.072 4.354-.2 6.782-2.618 6.979-6.98.059-1.28.073-1.689.073-4.948 0-3.259-.014-3.667-.072-4.947-.196-4.354-2.617-6.78-6.979-6.98-1.281-.059-1.69-.073-4.949-.073zm0 5.838c-3.403 0-6.162 2.759-6.162 6.162s2.759 6.163 6.162 6.163 6.162-2.759 6.162-6.163c0-3.403-2.759-6.162-6.162-6.162zm0 10.162c-2.209 0-4-1.79-4-4 0-2.209 1.791-4 4-4s4 1.791 4 4c0 2.21-1.791 4-4 4zm6.406-11.845c-.796 0-1.441.645-1.441 1.44s.645 1.44 1.441 1.44c.795 0 1.439-.645 1.439-1.44s-.644-1.44-1.439-1.44z" />
                  </svg>
                </a>
                <a href="https://www.facebook.com/bihotz.btz">
                  <svg
                    className="footer__fb"
                    width="32px"
                    height="33px"
                    viewBox="0 0 32 33"
                    version="1.1"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                      <g id="Homepage" transform="translate(-203.000000, -1359.000000)" fill="#000000" fillRule="nonzero">
                        <g id="facebook" transform="translate(203.000000, 1359.000000)">
                          <path
                            d="M32,16.5980707 C32,7.64095004 24.8387097,0.38372093 16,0.38372093 C7.16129032,0.38372093 0,7.64095004 0,16.5980707 C0,24.6908618 5.85096774,31.3988952 13.5,32.6162791 L13.5,21.2851946 L9.43548387,21.2851946 L9.43548387,16.5980707 L13.5,16.5980707 L13.5,13.0256833 C13.5,8.96228879 15.8870968,6.7177782 19.5432258,6.7177782 C21.2941935,6.7177782 23.1251613,7.03421954 23.1251613,7.03421954 L23.1251613,11.0224265 L21.1070968,11.0224265 C19.12,11.0224265 18.5,12.2725006 18.5,13.5546111 L18.5,16.5980707 L22.9374194,16.5980707 L22.2277419,21.2851946 L18.5,21.2851946 L18.5,32.6162791 C26.1490323,31.3988952 32,24.6908618 32,16.5980707 Z"
                            id="Path"
                          ></path>
                        </g>
                      </g>
                    </g>
                  </svg>
                </a>
              </div>
              <a href="mailto:contact@bihotz.co" className="footer__mail">
                contact@bihotz.co
              </a>
              <div href="mailto:martin@miza.io" className="footer__boss">
                Site web fièrement réalisé par <a href="mailto:martin@miza.io">Mizā Studio</a>.
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}
