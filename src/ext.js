import $ from 'jquery';

$(document).ready(function() {
  $(document).on('click', '.burger', function(event) {
    if ($(this).hasClass('open')) {
      $(this).removeClass('open');
      $('.hidden-menu').removeClass('show');
      $('.body').removeClass('disable');
    } else {
      $(this).addClass('open');
      $('.hidden-menu').addClass('show');
      $('.body').addClass('disable');
    }
  });

  $(document).on('click', '.menu__link, .arrow-left', function(event) {
    $('.body').removeClass('disable');
  });

  if ($(window).width() > 820) {
    $(document).on('mouseenter', '.bag__container', function() {
      $(this)
        .find('.bag__border')
        .removeClass('moveout');
      $(this)
        .find('.bag__border')
        .addClass('movein');
      $(this)
        .find('.bagimg')
        .removeClass('remscale');
      $(this)
        .find('.bagimg')
        .addClass('scale');
      $(this)
        .find('.bag__image')
        .removeClass('remoffset');
      $(this)
        .find('.bag__image')
        .addClass('offset');
    });

    $(document).on('mouseleave', '.bag__container', function() {
      $(this)
        .find('.bag__border')
        .removeClass('movein');
      $(this)
        .find('.bag__border')
        .addClass('moveout');
      $(this)
        .find('.bagimg')
        .removeClass('scale');
      $(this)
        .find('.bagimg')
        .addClass('remscale');
      $(this)
        .find('.bag__image')
        .removeClass('offset');
      $(this)
        .find('.bag__image')
        .addClass('remoffset');
    });

    $(document).on('mouseenter', '.bag__container', function() {
      $(this)
        .find('.bag__border--down')
        .removeClass('moveout2');
      $(this)
        .find('.bag__border--down')
        .addClass('movein2');
    });

    $(document).on('mouseleave', '.bag__container', function() {
      $(this)
        .find('.bag__border--down')
        .removeClass('movein2');
      $(this)
        .find('.bag__border--down')
        .addClass('moveout2');
    });
  }
});
