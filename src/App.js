import React from 'react';
import './App.css';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Home from './views/home/home';
import Mihi from './views/mihi/mihi';
import Kupela from './views/kupela/kupela';
import Zego from './views/zego/zego';
import Xerra from './views/xerra/xerra';
import ReactGA from 'react-ga';

ReactGA.initialize('UA-61351438-9');

const App = () => {
  return (
    <Route
      render={({ location }) => (
        <TransitionGroup>
          <CSSTransition key={location.key} timeout={450} classNames="fade">
            <div className="App">
              <Switch location={location}>
                <Route path="/sac/mihi" component={Mihi} />
                <Route path="/sac/xerra" component={Xerra} />
                <Route path="/sac/kupela" component={Kupela} />
                <Route path="/sac/zego" component={Zego} />
                <Route path="/" component={Home} />
              </Switch>
            </div>
          </CSSTransition>
        </TransitionGroup>
      )}
    />
  );
};

export default App;
